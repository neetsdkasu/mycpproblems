# FizzBuzzString16

https://www.hackerrank.com/contests/neetsdkasu-my-problems/challenges/fizzbuzzstring16


#### Problem Statement

`FizzBuzz16`を次のように定義します (※擬似コード)
```
function FizzBuzz16(i) {
    if (i % 15 == 0) {
        return "FizzBuzz"
    } else if (i % 3 == 0) {
        return "Fizz"
    } else if (i % 5 == 0) {
        return "Buzz"
    } else {
        return to_hex_string(i)
    }
}
```
`to_hex_string`は整数を16進表記(英字は大文字)の文字列に変換する関数です  
例えば`FizzBuzz16(14)`は`"E"`、`FizzBuzz16(15)`は`"FizzBuzz"`、`FizzBuzz16(16)`は`"10"`、`FizzBuzz16(247)`は`"F7"`になります  


`FizzBuzzString16`を次のように定義します (※擬似コード)
```
function FizzBuzzString16(n) {
    s := ""
    for i in 1 .. n {
        s += FizzBuzz16(i)
    }
    return s
}
```
例えば`FizzBuzzString16(4)`は`"12Fizz4"`、`FizzBuzzString16(16)`は`"12Fizz4BuzzFizz78FizzBuzzBFizzDEFizzBuzz10"`になります  


整数`X`と`Y`が与えられるので`FizzBuzzString16(10000000000)`の`X`文字目から`Y`文字目の間にある文字`F`の個数を求めてください  
 
例えば`X=1,Y=5`のとき`FizzBuzzString16(10000000000)`の`1`文字目から`5`文字目は`"12Fiz"`で文字`F`の数は`1`個となります  
例えば`X=3,Y=18`のとき`FizzBuzzString16(10000000000)`の`3`文字目から`18`文字目は`"Fizz4BuzzFizz78F"`で文字`F`の数は`3`個となります  



#### Input Format

```
X
Y
```
1行目に整数`X`が与えられます  
2行目に整数`Y`が与えられます  
  
 
#### Constraints

整数`X`、`Y`の範囲は`1 <= X <= Y <= 10,000,000,000`です


#### Output Format

`FizzBuzzString16(10000000000)`の`X`文字目から`Y`文字目の間にある文字`F`の個数を出力してください  


#### Sample #0

###### Input #0
```
1
5
```
###### Output #0
```
1
```
###### Explanation #0
`FizzBuzzString16(10000000000)`の`1`文字目から`5`文字目は`"12Fiz"`で文字`F`の数は`1`個となります  


#### Sample #1

###### Input #1
```
3
18
```
###### Output #1  
```
3
```
###### Explanation #1
`FizzBuzzString16(10000000000)`の`3`文字目から`18`文字目は`"Fizz4BuzzFizz78F"`で文字`F`の数は`3`個となります  


#### Sample #2

###### Input #2
```
4
11
```
###### Output #2
```
0
```
###### Explanation #2
`FizzBuzzString16(10000000000)`の`4`文字目から`11`文字目は`"izz4Buzz"`で文字`F`はありません  


#### Sample #3

###### Input #3
```
33
33
```
###### Output #3
```
1
```
###### Explanation #3
`FizzBuzzString16(10000000000)`の`33`文字目は`"F"`で文字`F`の数は`1`個となります  


#### Sample #4

###### Input #4
```
500
1000
```
###### Output #4
```
62
```


#### Sample #5

###### Input #5
```
1
10000000000
```
###### Output #5
```
890456082
```

