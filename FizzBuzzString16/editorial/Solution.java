import java.util.*;

public class Solution {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        long X = scanner.nextLong();
        long Y = scanner.nextLong();

        long answer = new Solution().solve(X, Y);

        System.out.println(answer);
    }

    long solve(long X, long Y) {
        long countFtoXm1 = countFtoPosition(X - 1L);
        long countFtoY = countFtoPosition(Y);
        long answer = countFtoY - countFtoXm1;
        return answer;
    }

    long countFtoPosition(long X) {
        if (X < 1L) {
            return 0L;
        }
        long N = findN(X);
        long count = countF(N);
        long offset = fizzBuzzString16Count(N - 1L) + 1L;
        String s = fizzBuzz16(N);
        for (int i = 0; offset + (long)i <= X; i++) {
            if (s.charAt(i) == 'F') {
                count++;
            }
        }
        return count;
    }

    long findN(long X) {
        long lower = 0L;
        long upper = X;
        while (lower + 1L < upper) {
            long middle = (lower + upper) / 2L;
            long count = fizzBuzzString16Count(middle);
            if (count < X) {
                lower = middle;
            } else {
                upper = middle;
            }
        }
        return upper;
    }

    long countF(long N) {
        if (N < 1L) {
            return 0L;
        }

        long[][][] DP = new long[10+1][150+16][10+1];

        {
            int j = 0, k = 0;
            for (int i = 0; i <= 10; i++) {
                int d = (int)(((N-1L) >> (4*(10-i))) & 15L);
                for (int t = 0; t < d; t++) {
                    DP[i][j+t][k] += 1L;
                }
                if (d == 0xF) {
                    k++;
                }
                j += d;
            }
            DP[10][j][k] += 1L;
        }

        for (int i = 0; i <= 9; i++) {
            for (int j = 0; j <= 150; j++) {
                for (int k = 0; k <= 9; k++) {
                    for (int d = 0x0; d <= 0xF; d++) {
                        if (d != 0xF) {
                            DP[i+1][j+d][k] += DP[i][j][k];
                        } else if (d == 0xF) {
                            DP[i+1][j+d][k+1] += DP[i][j][k];
                        }
                    }
                }
            }
        }

        long count = 0L;
        for (int j = 1; j <= 150; j++) {
            for (int k = 0; k <= 10; k++) {
                if (j % 15 == 0) {
                    count += DP[10][j][k];
                } else if (j % 3 == 0) {
                    count += DP[10][j][k];
                } else if (j % 5 == 0) {
                    count += 0L;
                } else {
                    count += (long)k * DP[10][j][k];
                }
            }
        }

        return count;
    }

    long fizzBuzzString16Count(long N) {
        long result = 0L;

        long digits = 1L;

        while (true) {
            long lower = (long)Math.pow(16.0, digits - 1.0);
            if (lower > N) break;
            long upper = Math.min(N, lower * 16L - 1L);

            long count = upper - (lower - 1L);
            long count15 = (upper / 15L) - ((lower - 1L) / 15L);
            long count3 = (upper / 3L) - ((lower - 1L) / 3L);
            long count5 = (upper / 5L) - ((lower - 1L) / 5L);

            long A = 8L * count15;
            long B = 4L * (count3 - count15);
            long C = 4L * (count5 - count15);
            long D = digits * (count - (count3 + count5 - count15));
            result += A + B + C + D;

            digits++;
        }

        return result;
    }

    String fizzBuzz16(long N) {
        if (N % 15L == 0L) {
            return "FizzBuzz";
        } else if (N % 3L == 0L) {
            return "Fizz";
        } else if (N % 5L == 0L) {
            return "Buzz";
        } else {
            return Long.toString(N, 16).toUpperCase();
        }
    }
}
