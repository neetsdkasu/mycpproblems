# FizzBuzzString-2

https://www.hackerrank.com/contests/neetsdkasu-my-problems/challenges/fizzbuzzstring-2


#### Problem Statement

`FizzBuzz`を次のように定義します (※擬似コード)
```
function FizzBuzz(i) {
    if (i % 15 == 0) {
        return "FizzBuzz"
    } else if (i % 3 == 0) {
        return "Fizz"
    } else if (i % 5 == 0) {
        return "Buzz"
    } else {
        return to_string(i)
    }
}
```
例えば`FizzBuzz(15)`は`"FizzBuzz"`、`FizzBuzz(16)`は`"16"`になります  


`FizzBuzzString`を次のように定義します (※擬似コード)
```
function FizzBuzzString(n) {
    s := ""
    for i in 1 .. n {
        s += FizzBuzz(i)
    }
    return s
}
```
例えば`FizzBuzzString(4)`は`"12Fizz4"`、`FizzBuzzString(7)`は`"12Fizz4BuzzFizz7"`になります  


整数`X`が与えられるので`FizzBuzzString(10000000000)`の`X`文字目の文字を答えてください   
 
`FizzBuzzString(10000000000)`の先頭から45文字分は`"12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz16 ..."`となります    
例えば`X=1`のとき`1`文字目の文字は`"1"`です、`X=4`のとき`4`文字目の文字は`"i"`です、`X=45`のとき`45`文字目の文字は`"6"`です    


#### Input Format

```
X
```
1行目に整数`X`が与えられます
  
 
#### Constraints

整数`X`の範囲は`1 <= X <= 10,000,000,000`です


#### Output Format

`FizzBuzzString(10000000000)`の`X`文字目の文字をを出力してください  


#### Sample #0

###### Input #0
```
1
```
###### Output #0
```
1
```
###### Explanation #0
`FizzBuzzString(10000000000)`の`1`文字目は`"1"`です  


#### Sample #1

###### Input #1
```
4
```
###### Output #1  
```
i
```
###### Explanation #1
`FizzBuzzString(10000000000)`の`4`文字目は`"i"`です  


#### Sample #2

###### Input #2
```
45
```
###### Output #2
```
6
```
###### Explanation #2
`FizzBuzzString(10000000000)`の`45`文字目は`"6"`です


#### Sample #3

###### Input #3
```
10000000000
```
###### Output #3
```
9
```

