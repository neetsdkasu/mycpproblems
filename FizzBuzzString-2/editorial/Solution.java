import java.util.*;

public class Solution {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        long X = scanner.nextLong();

        char answer = new Solution().solve(X);

        System.out.println(answer);
    }

    char solve(long X) {
        long lower = 0L;
        long upper = X;
        while (lower + 1L < upper) {
            long middle = (lower + upper) / 2L;
            long count = fizzBuzzStringCount(middle);
            if (count < X) {
                lower = middle;
            } else {
                upper = middle;
            }
        }
        long P = upper;
        int pos = (int)(X - fizzBuzzStringCount(P - 1L));
        char answer = fizzBuzz(P).charAt(pos - 1);
        return answer;
    }

    long fizzBuzzStringCount(long n) {
        long answer = 0L;

        long digits = 1L;

        while (true) {
            long lower = (long)Math.pow(10.0, digits - 1.0);
            if (lower > n) break;
            long upper = Math.min(n, lower * 10L - 1L);

            long count = upper - (lower - 1L);
            long count15 = (upper / 15L) - ((lower - 1L) / 15L);
            long count3 = (upper / 3L) - ((lower - 1L) / 3L);
            long count5 = (upper / 5L) - ((lower - 1L) / 5L);

            long A = 8L * count15;
            long B = 4L * (count3 - count15);
            long C = 4L * (count5 - count15);
            long D = digits * (count - (count3 + count5 - count15));
            answer += A + B + C + D;

            digits++;
        }

        return answer;
    }

    String fizzBuzz(long N) {
        if (N % 15L == 0L) {
            return "FizzBuzz";
        } else if (N % 3L == 0L) {
            return "Fizz";
        } else if (N % 5L == 0L) {
            return "Buzz";
        } else {
            return Long.toString(N);
        }
    }
}