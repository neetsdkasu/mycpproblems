## 解説

問題FizzBuzzString-1を解いていることを前提とします  

問題FizzBuzzString-1の解を求める関数を`FizzBuzzStringCount(N)`とします  
`X`文字目を含む数を`P`とします (例えば、`X=1`なら`P=1`、`X=4`なら`P=3`、`X=45`なら`P=16`が該当します)  
このとき`FizzBuzzStringCount(P-1) < X <= FizzBuzzStringCount(P)`が成立することが分かります  
`P`を特定すると`FizzBuzz(P)`の`(X-FizzBuzzStringCount(P-1))`文字目が求める答えとなります  
`P`は二分探索で特定できます    


##### Javaによる実装例
```java
    char solve(long X) {
        long lower = 0L;
        long upper = X;
        while (lower + 1L < upper) {
            long middle = (lower + upper) / 2L;
            long count = fizzBuzzStringCount(middle);
            if (count < X) {
                lower = middle;
            } else {
                upper = middle;
            }
        }
        long P = upper;
        int pos = (int)(X - fizzBuzzStringCount(P - 1L));
        char answer = fizzBuzz(P).charAt(pos - 1);
        return answer;
    }
```

