## 解説

文字列`S`の文字の位置の`mod K`ごとに英文字(A-Z)への変更を全部試して操作回数が最小になるものを合計すれば答えを求められます

```java
int solve(int n, int k, String s) {

    int answer = 0;

    for (int i = 0; i < k; i++) {
        int minimum = Integer.MAX_VALUE;

        for (char ch = 'A'; ch <= 'Z'; ch++) {
            int tmp = 0;
            int pos = i;
            while (pos < s.length()) {
                if (s.charAt(pos) != ch) {
                    tmp++;
                }
                pos += k;
            }
            minimum = Math.min(minimum, tmp);
        }

        answer += minimum;
    }

    return answer;
}
```