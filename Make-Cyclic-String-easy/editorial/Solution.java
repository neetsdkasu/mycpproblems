// Make Cyclic String - easy -
// author: Leonardone @ NEETSDKASU
import java.util.*;

class Solution {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int K = scanner.nextInt();
        String S = scanner.next();

        int answer = new Solution().solve(N, K, S);

        System.out.println(answer);
    }

    int solve(int n, int k, String s) {

        int answer = 0;

        for (int i = 0; i < k; i++) {
            int minimum = Integer.MAX_VALUE;

            for (char ch = 'A'; ch <= 'Z'; ch++) {
                int tmp = 0;
                int pos = i;
                while (pos < s.length()) {
                    if (s.charAt(pos) != ch) {
                        tmp++;
                    }
                    pos += k;
                }
                minimum = Math.min(minimum, tmp);
            }

            answer += minimum;
        }

        return answer;
    }
}
