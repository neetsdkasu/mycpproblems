## 解説

    2～8の数字が含まれてないことを確認する
    カードの種類の数を数えて13種類あるか確認する



1) 2～8の数字が含まれてないことを確認する

各カードを2文字の文字列として取得したとして
2文字目に2～8の数字の文字が存在するかを調べばよい
調べる手段はたくさんあるが3つほど例に挙げると

    2文字目を整数型に変換して2～8の間にあるか調べる
    正規表現で調べる
    文字探索関数(メソッド)を使って調べる

Javaでの上記3例の実装例
いずれも2～8が含まれているときtrueを返すメソッド

    boolean checkCardNumberBetween2and8ByInteger(String card) {
        int number = Integer.parseInt(card.substring(1));
        return 2 <= number && number <= 8;
    }
    
    boolean checkCardNumberBetween2and8ByRegExp(String card) {
        return card.matches(".[2-8]");
    }
    
    boolean checkCardNumberBetween2and8ByFindFunction(String card) {
        return "2345678".contains(card.substring(1));
    }
    



2) カードの種類の数を数えて13種類あるか確認する

種類を数える手段はたくさんあるが4つほど挙げると

    ソートして隣り合うカードが違う場合を数える
    Setと呼ばれるデータ構造を使って数える
    総当りで後方に重複がある場合を総数から引くことで数える
    重複を取り除く関数(メソッド)を用いて重複を取り除いてから数える

Java8での上記4例の実装例

    int countKindsBySort(String[] cards) {
        if (cards.length == 0) {
            return 0;
        }
        String[] sortedcards = java.util.Arrays.copyOf(cards, cards.length);
        java.util.Arrays.sort(sortedcards);
        int count = 1;
        for (int i = 1; i < sortedcards.length; i++) {
            if (!sortedcards[i].equals(sortedcards[i - 1])) {
                count++;
            }
        }
        return count;
    }
    
    int countKindsBySet(String[] cards) {
        java.util.Set<String> kinds = new java.util.HashSet<>();
        for (String card : cards) {
            kinds.add(card);
        }
        return kinds.size();
    }
    
    int countKindsByBruteForce(String[] cards) {
        int count = cards.length;
        for (int i = 0; i < cards.length; i++) {
            boolean found = false; 
            for (int j = i + 1; j < cards.length; j++) {
                found |= cards[i].equals(cards[j]);
            }
            if (found) {
                count--;
            }
        }
        return count;
    }
    
    int countKindsByUniquelyFunction(String[] cards) {
        return (int)java.util.Arrays.stream(cards).distinct().count();
    }
    