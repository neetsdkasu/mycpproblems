public class Solution {

    boolean checkCardNumberBetween2and8ByRegExp(String card) {
        return card.matches(".[2-8]");
    }

    int countKindsByUniquelyFunction(String[] cards) {
        return (int)java.util.Arrays.stream(cards).distinct().count();
    }
    
    boolean solve(String[] cards) {
        for (String card  : cards) {
            if (checkCardNumberBetween2and8ByRegExp(card)) {
                return false;
            }
        }
        return countKindsByUniquelyFunction(cards) == 13;
    }
    
    public static void main(String[] args) throws Exception {
        
        String[] cards = new java.util.Scanner(System.in).nextLine().split(" ");
        
        if (new Solution().solve(cards)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
