# Kokushimuso-1


#### Problem Statement

This is a card game.   
  
One alphabet and one digit are printed on card surface.  
There are 34 types of card.  
```
M1 M2 M3 M4 M5 M6 M7 M8 M9
P1 P2 P3 P4 P5 P6 P7 P8 P9
S1 S2 S3 S4 S5 S6 S7 S8 S9
A0 B0 C0 D0 X0 Y0 Z0
```
Total 136 cards of the four copies various will be provided in the game.  
136 cards from well inner shuffled are dealt 14 sheets as your hand.  
(The remaining 122 cards, etc., must be not to think because not addressed in this issue.)  
In this case the role of Kokushimuso (thirteen orphans) when it meets the two conditions shown hand is then at the same time is satisfied.  
```
- That the hand of 14 cards does not include the cards that was written one of the number of 2 to 8

- To be a 13 kind when you divide the 14 cards of the hand by type
```
  
Please determine whether the Kokushimuso has been established in the 14 cards that were distributed as a hand. 


#### Input Format

```
Card1 Card2 … Card14
```
You dealt the hand 14 cards will be given with a space-delimited.
One card will be a total of two letters of the alphabet one letter-single digit.  
Alphabetic characters to be used is the A B C D M P S X Y Z.  
Number used is 0 1 2 3 4 5 6 7 8 9.  
   

#### Constraints

Please refer to the problem statement about the type of card (kind of a combination of letters and numbers).  


#### Output Format

Please determine whether the Kokushimuso has been established in the dealt the 14 cards.  
Please if you Kokushimuso are satisfied by outputting a `YES`.  
If the Kokushimuso is not satisfied, please output the `NO`.  
And, please output a newline.  


#### Sample Input

Test Case #0  
```
C0 M1 S9 A0 Y0 P9 P1 B0 Z0 M9 D0 S1 P9 X0
```


Test Case #1  
```
M1 M2 A0 S1 B0 S3 X0 Z0 M9 C0 D0 Y0 P9 P1
```


Test Case #2  
```
S1 S1 A0 M9 B0 B0 Z0 M1 P9 X0 P1 D0 S9 S1
```


Test Case #3  
```
D0 M9 C0 A0 M1 X0 Y0 P9 P1 P1 S1 Z0 S9 B0
```


Test Case #4  
```
P6 M8 P5 P2 M6 S5 Z0 M6 M7 Y0 Y0 M9 P9 M5
```


#### Sample Output

Test Case #0  
```
YES
```


Test Case #1  
```
NO
```


Test Case #2
```
NO
```


Test Case #3  
```
YES
```


Test Case #4  
```
NO
```

#### Explanation

Test Case #0  
Hand 14 cards do not contain a number of cards written 2-8,  
Kokushimuso is satisfied because the type of card is 13 kinds.  
     
    
Test Case #1  
Since there are 14 cards to the card it was written a number of 2 ~ 8 (M2 and S3) does not satisfied Kokushimuso.  


Test Case #2  
Although the hand does not include a card that was written a number of 2 to 8,  
Since the type of card is not enough to 11 types and 13 kinds Kokushimuso is not satisfied.  


