import java.util.*;

public class Solution {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        long N = scanner.nextLong();

        long answer = new Solution().solve(N);

        System.out.println(answer);
    }

    long solve(long n) {
        long answer = 0L;

        long digits = 1L;

        while (true) {
            long lower = (long)Math.pow(10.0, digits - 1.0);
            if (lower > n) break;
            long upper = Math.min(n, lower * 10L - 1L);

            long count = upper - (lower - 1L);
            long count15 = (upper / 15L) - ((lower - 1L) / 15L);
            long count3 = (upper / 3L) - ((lower - 1L) / 3L);
            long count5 = (upper / 5L) - ((lower - 1L) / 5L);

            long A = 8L * count15;
            long B = 4L * (count3 - count15);
            long C = 4L * (count5 - count15);
            long D = digits * (count - (count3 + count5 - count15));
            answer += A + B + C + D;

            digits++;
        }

        return answer;
    }
}