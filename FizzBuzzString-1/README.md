# FizzBuzzString-1

https://www.hackerrank.com/contests/neetsdkasu-my-problems/challenges/fizzbuzzstring-1


#### Problem Statement

`FizzBuzz`を次のように定義します (※擬似コード)
```
function FizzBuzz(i) {
    if (i % 15 == 0) {
        return "FizzBuzz"
    } else if (i % 3 == 0) {
        return "Fizz"
    } else if (i % 5 == 0) {
        return "Buzz"
    } else {
        return to_string(i)
    }
}
```
例えば`FizzBuzz(15)`は`"FizzBuzz"`、`FizzBuzz(16)`は`"16"`になります  


`FizzBuzzString`を次のように定義します (※擬似コード)
```
function FizzBuzzString(n) {
    s := ""
    for i in 1 .. n {
        s += FizzBuzz(i)
    }
    return s
}
```
例えば`FizzBuzzString(4)`は`"12Fizz4"`、`FizzBuzzString(7)`は`"12Fizz4BuzzFizz7"`になります  


整数`N`が与えられるので`FizzBuzzString(N)`の文字数を求めてください  
 
例えば`N=4`のとき`FizzBuzzString(4)`は`7`文字、`N=7`のとき`FizzBuzzString(7)`は`16`文字となります  


#### Input Format

```
N
```
1行目に整数`N`が与えられます
  
 
#### Constraints

整数`N`の範囲は`1 <= N <= 10,000,000,000`です


#### Output Format

```
L
```
`FizzBuzzString(N)`の文字数`L`を出力してください  


#### Sample #0

###### Input #0
```
1
```
###### Output #0
```
1
```
###### Explanation #0
`FizzBuzzString(1)`は`"1"`になるので文字数は`1`です  


#### Sample #1

###### Input #1
```
4
```
###### Output #1  
```
7
```
###### Explanation #1
`FizzBuzzString(4)`は`"12Fizz4"`になるので文字数は`7`です  


#### Sample #2

###### Input #2
```
16
```
###### Output #2
```
45
```
###### Explanation #2
`FizzBuzzString(16)`は`"12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz16"`になるので文字数は`45`です  


#### Sample #3

###### Input #3
```
10000000000
```
###### Output #3
```
74074074073
```

