// Make Cyclic String
// author: Leonardone @ NEETSDKASU

#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;

// 下記サイトを参考にした
// h ttps://cp-algorithms.com/graph/min_cost_flow.html
namespace minimum_cost_flow {
    using Flow = int;
    using Cost = int;
    const Cost INF = 1e9;

    class Graph {
        int n;
        vector<vector<int>> adj;
        vector<vector<Flow>> capacity;
        vector<vector<Cost>> cost;
        void shortest_paths(int v0, vector<Cost>& dist, vector<int>& parent);
    public:
        Graph(int n): n(n) {
            this->adj.assign(n, vector<int>());
            this->capacity.assign(n, vector<Flow>(n, 0));
            this->cost.assign(n, vector<Cost>(n, 0));
        }
        void add_edge(int from, int to, Flow capacity, Cost cost);
        Cost calc(Flow k, int src, int sink);
    };

    void Graph::add_edge(int from, int to, Flow capacity, Cost cost) {
        this->adj[from].push_back(to);
        this->adj[to].push_back(from);
        this->cost[from][to] = cost;
        this->cost[to][from] = -cost;
        this->capacity[from][to] = capacity;
    }

    Cost Graph::calc(Flow k, int src, int sink) {
        Flow flow = 0;
        Cost cost = 0;
        vector<Cost> dist(this->n, 0);
        vector<int> parent(this->n, 0);
        while (flow < k) {
            this->shortest_paths(src, dist, parent);
            if (dist[sink] == INF) break;
            Flow f = k - flow;
            int cur = sink;
            while (cur != src) {
                f = min(f, this->capacity[parent[cur]][cur]);
                cur = parent[cur];
            }
            flow += f;
            cost += (Cost)f * dist[sink];
            cur = sink;
            while (cur != src) {
                this->capacity[parent[cur]][cur] -= f;
                this->capacity[cur][parent[cur]] += f;
                cur = parent[cur];
            }
        }
        return flow < k ? -1 : cost;
    }

    void Graph::shortest_paths(int v0, vector<Cost>& dist, vector<int>& parent) {
        parent.assign(this->n, this->n+1);
        dist.assign(this->n, INF);
        dist[v0] = 0;
        vector<bool> in_que(this->n, false);
        queue<int> que;
        que.push(v0);
        while (!que.empty()) {
            int u = que.front();
            que.pop();
            in_que[u] = false;
            for (int v : this->adj[u]) {
                if (this->capacity[u][v] <= 0) continue;
                if (dist[v] <= dist[u] + this->cost[u][v]) continue;
                dist[v] = dist[u] + this->cost[u][v];
                parent[v] = u;
                if (!in_que[v]) {
                    in_que[v] = true;
                    que.push(v);
                }
            }
        }
    }
}

int main() {

    int n, k;
    string s;
    cin >> n >> k >> s;

    int matrix[26][26] = {};
    for (int i = 0; i < n; i++) {
        int ch = (int)s[i] - 'A';
        int j = i % k;
        for (int x = 0; x < 26; x++) {
            if (ch != x) {
                matrix[j][x]++;
            }
        }
    }

    minimum_cost_flow::Graph graph(k+28);

    // i文字目 -> 文字x
    for (int i = 0; i < k; i++) {
        for (int x = 0; x < 26; x++) {
            graph.add_edge(i, k+x, 1, matrix[i][x]);
        }
    }
    int src = k + 26;
    int sink = src + 1;
    // 流入口src -> i文字目
    for (int i = 0; i < k; i++) {
        graph.add_edge(src, i, 1, 0);
    }
    // 文字x -> 流出口sink
    for (int x = 0; x < 26; x++) {
        graph.add_edge(k+x, sink, 1, 0);
    }

    auto ans = graph.calc(k, src, sink);

    cout << ans << endl;

    return 0;
}
