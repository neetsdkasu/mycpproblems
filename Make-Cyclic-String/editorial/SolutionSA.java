// Make Cyclic String
// 焼きなまし法っぽいアプローチ
// author: Leonardone @ NEETSDKASU
import java.util.*;

public class SolutionSA {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int K = scanner.nextInt();
        String S = scanner.next();

        int answer = new SolutionSA().solve(N, K, S);

        System.out.println(answer);
    }

    int solve(int n, int k, String s) {

        // 文字変更時の操作回数をまとめる
        int[][] table = new int[k][26];
        for (int i = 0; i < n; i++) {
            int ch = (int)s.charAt(i) - 'A';
            int j = i % k;
            for (int x = 0; x < 26; x++) {
                if (x != ch) {
                    table[j][x]++;
                }
            }
        }

        int answer = 0;

        // アルファベットの順列を作る
        int[] seq = new int[26];
        for (int i = 0; i < 26; i++) {
            seq[i] = i;
            if (i < k) {
                answer += table[i][i];
            }
        }

        Random rand = new Random(-111L);
        int tmp = answer;
        int CYCLE = 200000;
        for (int multi = 0; multi < 50; multi++) {

            // 焼きなまし法を用いる箇所
            for (int cycle = 0; cycle < CYCLE; cycle++) {
                int i = rand.nextInt(Math.min(k, 25));
                int j = rand.nextInt(25 - i) + i + 1;
                int chi = seq[i];
                int chj = seq[j];
                int beforei = table[i][chi];
                int afteri = table[i][chj];
                int next;
                if (j < k) {
                    int beforej = table[j][chj];
                    int afterj = table[j][chi];
                    next = tmp + (afteri + afterj) - (beforei + beforej);
                } else {
                    next = tmp + afteri - beforei;
                }
                if (next <= tmp || rand.nextDouble() <= probability(tmp, next, temperature(cycle, CYCLE))) {
                    seq[i] = chj;
                    seq[j] = chi;
                    tmp = next;
                    if (tmp < answer) {
                        answer = tmp;
                    }
                }
            }

            // 新しい初期状態を作る (多スター トぽくしている)
            for (int i = 25; i > 0; i--) {
                int j = rand.nextInt(i);
                int t = seq[i];
                seq[i] = seq[j];
                seq[j] = t;
            }
            tmp = 0;
            for (int i = 0; i < k; i++) {
                tmp += table[i][seq[i]];
            }
        }

        return answer;
    }

    // Wikipediaを参考に焼きなまし法を実装
    // h ttps://ja.wikipedia.org/wiki/焼きなまし法

    double probability(int e1, int e2, double t) {
        if (e1 >= e2) {
            return 1.0;
        } else {
            return Math.exp((double)(e1 - e2) /  t);
        }
    }

    double temperature(int iter, int maxIter) {
        double r = (double)iter / (double)maxIter;
        return Math.pow(0.1, r);
    }
}