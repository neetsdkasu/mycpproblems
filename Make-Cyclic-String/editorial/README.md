## 解説

1文字目～K文字目と文字A～文字Zのの組み合わせを考えると  
2部グラフの最大マッチングの問題とみなせて、最小費用流などで答えを求められます  

※入力のKが小さいため、焼きなまし法などのヒューリスティクスなアプローチや嘘解法でもACできるかもしれません…  

![image](data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNjAiIGhlaWdodD0iMzgwIiB2aWV3Qm94PSIwIDAgMjYwIDM4MCI+PGNpcmNsZSBjeD0iMzAiIGN5PSI1MCIgcj0iMzAiIHN0cm9rZT0iYmxhY2siIGZpbGw9IndoaXRlIi8+PGNpcmNsZSBjeD0iMzAiIGN5PSIxMjAiIHI9IjMwIiBzdHJva2U9ImJsYWNrIiBmaWxsPSJ3aGl0ZSIvPjxjaXJjbGUgY3g9IjMwIiBjeT0iMTkwIiByPSIzMCIgc3Ryb2tlPSJibGFjayIgZmlsbD0id2hpdGUiLz48bGluZSB4MT0iMzAiIHkxPSIyMzAiIHgyPSIzMCIgeTI9IjI5MCIgc3Ryb2tlPSJibGFjayIgc3Ryb2tlLWRhc2hhcnJheT0iMSAyIiAvPjxjaXJjbGUgY3g9IjMwIiBjeT0iMzMwIiByPSIzMCIgc3Ryb2tlPSJibGFjayIgZmlsbD0id2hpdGUiLz48Y2lyY2xlIGN4PSIyMzAiIGN5PSI1MCIgcj0iMzAiIHN0cm9rZT0iYmxhY2siIGZpbGw9IndoaXRlIi8+PGNpcmNsZSBjeD0iMjMwIiBjeT0iMTIwIiByPSIzMCIgc3Ryb2tlPSJibGFjayIgZmlsbD0id2hpdGUiLz48Y2lyY2xlIGN4PSIyMzAiIGN5PSIxOTAiIHI9IjMwIiBzdHJva2U9ImJsYWNrIiBmaWxsPSJ3aGl0ZSIvPjxsaW5lIHgxPSIyMzAiIHkxPSIyMzAiIHgyPSIyMzAiIHkyPSIyOTAiIHN0cm9rZT0iYmxhY2siIHN0cm9rZS1kYXNoYXJyYXk9IjEgMiIgLz48Y2lyY2xlIGN4PSIyMzAiIGN5PSIzMzAiIHI9IjMwIiBzdHJva2U9ImJsYWNrIiBmaWxsPSJ3aGl0ZSIvPjxsaW5lIHgxPSI1NyIgeTE9IjU5IiB4Mj0iMjAzIiB5Mj0iMTExIiBzdHJva2U9ImJsYWNrIiAvPjxsaW5lIHgxPSI2MCIgeTE9IjE5MCIgeDI9IjIwMCIgeTI9IjE5MCIgc3Ryb2tlPSJibGFjayIgLz48bGluZSB4MT0iNTAiIHkxPSIxNDEiIHgyPSIxMjAiIHkyPSIyMTUiIHN0cm9rZT0iYmxhY2siIHN0cm9rZS1kYXNoYXJyYXk9IjkwIDIgMiAyIDIgMiIgLz48bGluZSB4MT0iMjEzIiB5MT0iNzQiIHgyPSIxNDAiIHkyPSIxNzYiIHN0cm9rZT0iYmxhY2siIHN0cm9rZS1kYXNoYXJyYXk9IjExNSAyIDIgMiAyIDIiIC8+PGxpbmUgeDE9IjQ3IiB5MT0iMzA2IiB4Mj0iOTAiIHkyPSIyNDYiIHN0cm9rZT0iYmxhY2siIHN0cm9rZS1kYXNoYXJyYXk9IjYwIDIgMiAyIDIgMiIgLz48bGluZSB4MT0iMjEwIiB5MT0iMzA5IiB4Mj0iMTcwIiB5Mj0iMjY3IiBzdHJva2U9ImJsYWNrIiBzdHJva2UtZGFzaGFycmF5PSI0NSAyIDIgMiAyIDIiIC8+PHRleHQgeD0iNyIgeT0iNTUiIGZvbnQtc2l6ZT0iMTMiPjHmloflrZfnm648L3RleHQ+PHRleHQgeD0iNyIgeT0iMTI1IiBmb250LXNpemU9IjEzIj4y5paH5a2X55uuPC90ZXh0Pjx0ZXh0IHg9IjciIHk9IjE5NSIgZm9udC1zaXplPSIxMyI+M+aWh+Wtl+ebrjwvdGV4dD48dGV4dCB4PSI3IiB5PSIzMzUiIGZvbnQtc2l6ZT0iMTMiPkvmloflrZfnm648L3RleHQ+PHRleHQgeD0iMjEwIiB5PSI1NSIgZm9udC1zaXplPSIxMyI+5paH5a2XQTwvdGV4dD48dGV4dCB4PSIyMTAiIHk9IjEyNSIgZm9udC1zaXplPSIxMyI+5paH5a2XQjwvdGV4dD48dGV4dCB4PSIyMTAiIHk9IjE5NSIgZm9udC1zaXplPSIxMyI+5paH5a2XQzwvdGV4dD48dGV4dCB4PSIyMTAiIHk9IjMzNSIgZm9udC1zaXplPSIxMyI+5paH5a2XWjwvdGV4dD48L3N2Zz4)


Javaによる実装例
```java
// Make Cyclic String
// author: Leonardone @ NEETSDKASU
import java.util.*;

class Solution {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int K = scanner.nextInt();
        String S = scanner.next();

        int answer = new Solution().solve(N, K, S);

        System.out.println(answer);
    }

    int solve(int n, int k, String s) {

        int[][] costTable = new int[k][26];
        for (int i = 0; i < n; i++) {
            int ch = (int)s.charAt(i) - 'A';
            int j = i % k;
            for (int x = 0; x < 26; x++) {
                if (ch != x) costTable[j][x]++;
            }
        }

        MinimumCostFlow MCF = new MinimumCostFlow(k + 28);

        int src = k + 26;
        int sink = src + 1;

        for (int i = 0; i < k; i++) {
            MCF.addEdge(src, i, 1, 0);
        }
        for (int i = 0; i < k; i++) {
            for (int x = 0; x < 26; x++) {
                MCF.addEdge(i, k + x, 1, costTable[i][x]);
            }
        }
        for (int x = 0; x < 26; x++) {
            MCF.addEdge(k + x, sink, 1, 0);
        }

        return MCF.calc(k, src, sink);
    }
}

// 下記サイトを参考にした
// h ttps://cp-algorithms.com/graph/min_cost_flow.html
class MinimumCostFlow {
    static int INF = 1_000_000_000;
    int n;
    ArrayList<ArrayList<Integer>> adj;
    int[][] capacity, cost;
    public MinimumCostFlow(int n) {
        this.n = n;
        this.adj = new ArrayList<>();
        for (int i = 0; i < n; i++) this.adj.add(new ArrayList<>());
        this.capacity = new int[n][n];
        this.cost = new int[n][n];
    }
    public void addEdge(int from, int to, int capacity, int cost) {
        this.adj.get(from).add(to);
        this.adj.get(to).add(from);
        this.cost[from][to] = cost;
        this.cost[to][from] = -cost;
        this.capacity[from][to] = capacity;
    }
    public int calc(int k, int src, int sink) {
        int flow = 0;
        int cost = 0;
        int[] dist = new int[this.n], parent = new int[this.n];
        while (flow < k) {
            this.shortestPaths(src, dist, parent);
            if (dist[sink] == INF) break;
            int f = k - flow;
            int cur = sink;
            while (cur != src) {
                f = Math.min(f, this.capacity[parent[cur]][cur]);
                cur = parent[cur];
            }
            flow += f;
            cost += f * dist[sink];
            cur = sink;
            while (cur != src) {
                this.capacity[parent[cur]][cur] -= f;
                this.capacity[cur][parent[cur]] += f;
                cur = parent[cur];
            }
        }
        return flow < k ? -1 : cost;
    }
    void shortestPaths(int v0, int[] dist, int[] parent) {
        Arrays.fill(parent, this.n + 1);
        Arrays.fill(dist, INF);
        dist[v0] = 0;
        boolean[] in_que = new boolean[this.n];
        ArrayDeque<Integer> que = new ArrayDeque<>();
        que.addLast(v0);
        while (!que.isEmpty()) {
            int u = que.removeFirst();
            in_que[u] = false;
            for (int v : this.adj.get(u)) {
                if (this.capacity[u][v] <= 0) continue;
                if (dist[v] <= dist[u] + this.cost[u][v]) continue;
                dist[v] = dist[u] + this.cost[u][v];
                parent[v] = u;
                if (!in_que[v]) {
                    in_que[v] = true;
                    que.addLast(v);
                }
            }
        }
    }
}
```

Pythonによる実装例(※scipyが使えることが前提)(※HackerRankではscipyは使えないかも)
```python
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import min_weight_full_bipartite_matching
 
n, k = map(int, input().split())
s = input()

# 文字変更時の操作回数の合計を辺の重みとする
matrix = [[1]*26 for _ in range(k)]
for i in range(n):
    ch = ord(s[i]) - ord("A")
    j = i % k
    for x in range(26):
        if x != ch:
            matrix[j][x] += 1
 
graph = csr_matrix(matrix)
answer = graph[min_weight_full_bipartite_matching(graph)].sum() - k
print(answer)
```