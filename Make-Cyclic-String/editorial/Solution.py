from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import min_weight_full_bipartite_matching
 
n, k = map(int, input().split())
s = input()

# 文字変更時の操作回数の合計を辺の重みとする
matrix = [[1]*26 for _ in range(k)]
for i in range(n):
    ch = ord(s[i]) - ord("A")
    j = i % k
    for x in range(26):
        if x != ch:
            matrix[j][x] += 1
 
graph = csr_matrix(matrix)
answer = graph[min_weight_full_bipartite_matching(graph)].sum() - k
print(answer)
