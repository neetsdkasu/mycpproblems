// Make Cyclic String
// author: Leonardone @ NEETSDKASU
import java.util.*;

class Solution {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int K = scanner.nextInt();
        String S = scanner.next();

        int answer = new Solution().solve(N, K, S);

        System.out.println(answer);
    }

    int solve(int n, int k, String s) {

        int[][] costTable = new int[k][26];
        for (int i = 0; i < n; i++) {
            int ch = (int)s.charAt(i) - 'A';
            int j = i % k;
            for (int x = 0; x < 26; x++) {
                if (ch != x) costTable[j][x]++;
            }
        }

        MinimumCostFlow MCF = new MinimumCostFlow(k + 28);

        int src = k + 26;
        int sink = src + 1;

        for (int i = 0; i < k; i++) {
            MCF.addEdge(src, i, 1, 0);
        }
        for (int i = 0; i < k; i++) {
            for (int x = 0; x < 26; x++) {
                MCF.addEdge(i, k + x, 1, costTable[i][x]);
            }
        }
        for (int x = 0; x < 26; x++) {
            MCF.addEdge(k + x, sink, 1, 0);
        }

        return MCF.calc(k, src, sink);
    }
}

// 下記サイトを参考にした
// h ttps://cp-algorithms.com/graph/min_cost_flow.html
class MinimumCostFlow {
    static int INF = 1_000_000_000;
    int n;
    ArrayList<ArrayList<Integer>> adj;
    int[][] capacity, cost;
    public MinimumCostFlow(int n) {
        this.n = n;
        this.adj = new ArrayList<>();
        for (int i = 0; i < n; i++) this.adj.add(new ArrayList<>());
        this.capacity = new int[n][n];
        this.cost = new int[n][n];
    }
    public void addEdge(int from, int to, int capacity, int cost) {
        this.adj.get(from).add(to);
        this.adj.get(to).add(from);
        this.cost[from][to] = cost;
        this.cost[to][from] = -cost;
        this.capacity[from][to] = capacity;
    }
    public int calc(int k, int src, int sink) {
        int flow = 0;
        int cost = 0;
        int[] dist = new int[this.n], parent = new int[this.n];
        while (flow < k) {
            this.shortestPaths(src, dist, parent);
            if (dist[sink] == INF) break;
            int f = k - flow;
            int cur = sink;
            while (cur != src) {
                f = Math.min(f, this.capacity[parent[cur]][cur]);
                cur = parent[cur];
            }
            flow += f;
            cost += f * dist[sink];
            cur = sink;
            while (cur != src) {
                this.capacity[parent[cur]][cur] -= f;
                this.capacity[cur][parent[cur]] += f;
                cur = parent[cur];
            }
        }
        return flow < k ? -1 : cost;
    }
    void shortestPaths(int v0, int[] dist, int[] parent) {
        Arrays.fill(parent, this.n + 1);
        Arrays.fill(dist, INF);
        dist[v0] = 0;
        boolean[] in_que = new boolean[this.n];
        ArrayDeque<Integer> que = new ArrayDeque<>();
        que.addLast(v0);
        while (!que.isEmpty()) {
            int u = que.removeFirst();
            in_que[u] = false;
            for (int v : this.adj.get(u)) {
                if (this.capacity[u][v] <= 0) continue;
                if (dist[v] <= dist[u] + this.cost[u][v]) continue;
                dist[v] = dist[u] + this.cost[u][v];
                parent[v] = u;
                if (!in_que[v]) {
                    in_que[v] = true;
                    que.addLast(v);
                }
            }
        }
    }
}