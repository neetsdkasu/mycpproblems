# Make Cyclic String

https://www.hackerrank.com/contests/neetsdkasu-my-problems/challenges/make-cyclic-string


#### Problem Statement

英大文字(A-Z)で構成された長さ`N`の文字列`S`が与えられます  

次の操作を1回の操作とします。この操作を何回でも行うことができるとします  

 - 文字列`S`の`i`文字目を任意の英大文字(A-Z)に変更する  (`1 <= i <= N`)  

整数`K`が与えられるので以下のルールを全て満たす文字列に変更するための最小の操作回数を求めてください

 - `i`文字目と`(i+K)`文字目が同じ文字 (`1 <= i <= N - K`)  
 - `i`文字目と`(i+p)`文字目は異なる文字 (`1 <= p < K`, `1 <= i <= N - p`)


#### Input Format

```
N K
S
```
1行目に整数`N`と`K`が半角スペース区切りで与えられます  
2行目に文字列`S`が与えられます  
  
 
#### Constraints

`1 <= K <= 26`  
`2*K <= N <= 100000`  
`N`は`K`の倍数  
`S`の長さは`N`  
`S`は英大文字(A-Z)で構成される  
入力の`S`はルールを満たさない

#### Output Format

ルールを満たすよう変更するための最小の操作回数を出力してください  


#### Sample #0

###### Input #0
```
15 3
AXCABCYBCABZABC
```
###### Output #0
```
3
```
###### Explanation #0
2文字目のXをB、7文字目のYをA、12文字目のZをCに変更すると
文字列`S`はABCABCABCABCABCになり
ルールを満たす最小の操作になります


#### Sample #1

###### Input #1
```
6 3
AAAAAA
```
###### Output #1  
```
4
```
###### Explanation #1
ルールを満たす最小の変更には例えばUSAUSAやABCABCなどがあります  


#### Sample #2

###### Input #2
```
10 1
JAPANJAPAN
```
###### Output #2
```
6
```
###### Explanation #2
全てのJ,P,NをAに変更することがルールを満たす最小の操作になります


#### Sample #3

###### Input #3
```
26 13
ABCDEFGHIJKLMNOPQRSTUVWXYZ
```
###### Output #3
```
13
```


#### Sample #4

###### Input #4
```
60 6
BDHACEDAAGDCCCFDFBCECCDCEGADFAHBHBHAFACGHBEBCAABHHHGGFBEDADH
```
###### Output #4
```
44
```

