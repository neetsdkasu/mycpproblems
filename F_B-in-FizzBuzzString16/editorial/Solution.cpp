#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <string>

// F,B in FizzBuzzString16
// author: Leonardone @ NEETSDKASU

using namespace std;

string to_hex_string(int64_t i) {
    stringstream s;
    s << std::uppercase << std::hex << i;
    return s.str();
}

string FizzBuzz16(int64_t i) {
    if (i % 15 == 0) {
        return "FizzBuzz";
    } else if (i % 3 == 0) {
        return "Fizz";
    } else if (i % 5 == 0) {
        return "Buzz";
    } else {
        return to_hex_string(i);
    }
}

string to_hex_string(int64_t i, int width) {
    stringstream s;
    s.width(width);
    s.fill('0');
    s << std::uppercase << std::hex << i;
    return s.str();
}

// FizzBuzzString16(v)のFとBの個数の合計を求める
int64_t CountFBofFizzBuzzString16(int64_t v) {
    if (v < 1) {
        return 0;
    }

    // 桁の組み合わせの総数
    // DP[桁数][桁の和][F,Bの個数]
    int64_t DP[10][150+16][11] = {};

    // DPの初期値の設定
    {
        int b = 0, c = 0;
        for (int a = 0; a < 10; a++) {
            int d = stoi(to_hex_string(v, 10).substr(a, 1), nullptr, 16);
            for (int t = 0; t < d; t++) {
                if (t == 0xB) {
                    DP[a][b+t][c+1] += 1;
                } else {
                    DP[a][b+t][c] += 1;
                }
            }
            if (d == 0xF || d == 0xB) {
                c++;
            }
            b += d;
        }
        DP[9][b][c] += 1;
    }

    for (int a = 0; a < 9; a++) {
        for (int b = 0; b < 150; b++) {
            for (int c = 0; c < 10; c++) {
                for (int d = 0x0; d < 0x10; d++) {
                    if (d == 0xF || d == 0xB) {
                        DP[a+1][b+d][c+1] += DP[a][b][c];
                    } else {
                        DP[a+1][b+d][c] += DP[a][b][c];
                    }
                }
            }
        }
    }

    int64_t count = 0;
    for (int b = 1; b < 150; b++) {
        for (int c = 0; c < 11; c++) {
            if (b % 15 == 0) {
                count += 2 * DP[9][b][c];
            } else if (b % 3 == 0) {
                count += DP[9][b][c];
            } else if (b % 5 == 0) {
                count += DP[9][b][c];
            } else {
                count += (int64_t)c * DP[9][b][c];
            }
        }
    }

    return count;
}

// FizzBuzzString16(v)の文字列長を求める
int64_t FizzBuzzString16Length(int64_t v) {
    if (v < 0) {
        return 0;
    }

    int64_t length = 0;
    int64_t digit_size = 1;

    while (true) {
        int64_t lower = (int64_t)pow(16.0, digit_size - 1.0);
        if (lower > v) {
            return length;
        }
        int64_t upper = min(v, lower * 16 - 1);

        int64_t count15 = (upper / 15) - ((lower - 1) / 15);
        int64_t count3 = (upper / 3) - ((lower - 1) / 3);
        int64_t count5 = (upper / 5) - ((lower - 1) / 5);
        int64_t count = upper - (lower - 1L);

        int64_t fizzbuzz = 8 * count15;
        int64_t fizz = 4 * (count3 - count15);
        int64_t buzz = 4 * (count5 - count15);
        int64_t hex_number = digit_size * (count - (count3 + count5 - count15));
        length += fizzbuzz + fizz + buzz + hex_number;

        digit_size++;
    }
}

// FizzBuzzString16(1000000000000)のv文字目において
//   FizzBuzzString16Length(N - 1) < v <= FizzBuzzString16Length(N)
// となるNを求める
int64_t FindNfromPosition(int64_t v) {
    int64_t lower = 0;
    int64_t upper = 1000000000001;
    while (lower + 1 < upper) {
        int64_t middle = (lower + upper) / 2;
        int64_t count = FizzBuzzString16Length(middle);
        if (count < v) {
            lower = middle;
        } else {
            upper = middle;
        }
    }
    return upper;
}

// FizzBuzzString16(1000000000000)の1文字目からv文字目までのFとBの個数の合計を求める
int64_t CountFBtoPosition(int64_t v) {
    if (v < 1) {
        return 0;
    }
    int64_t N = FindNfromPosition(v);
    int64_t count = CountFBofFizzBuzzString16(N - 1);
    int length = (int)(v - FizzBuzzString16Length(N - 1));
    string s = FizzBuzz16(N);
    for (int i = 0; i < length; i++) {
        if (s[i] == 'F' || s[i] == 'B') {
            count++;
        }
    }
    return count;
}

int64_t solve(int64_t X, int64_t Y) {
    int64_t a = CountFBtoPosition(Y);
    int64_t b = CountFBtoPosition(X - 1);
    int64_t answer = a - b;
    return answer;
}

int main() {
    int64_t X, Y, answer;
    cin >> X;
    cin >> Y;
    answer = solve(X, Y);
    cout << answer << endl;
    return 0;
}