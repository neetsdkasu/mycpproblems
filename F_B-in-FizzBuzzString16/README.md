# F,B in FizzBuzzString16

https://www.hackerrank.com/contests/neetsdkasu-my-problems/challenges/fb-in-fizzbuzzstring16


#### Problem Statement

`FizzBuzz16`を次のように定義します (※擬似コード)
```
function FizzBuzz16(i) {
    if (i % 15 == 0) {
        return "FizzBuzz"
    } else if (i % 3 == 0) {
        return "Fizz"
    } else if (i % 5 == 0) {
        return "Buzz"
    } else {
        return to_hex_string(i)
    }
}
```
`to_hex_string`は整数を16進表記(英字は大文字)の文字列に変換する関数です  
例えば`FizzBuzz16(14)`は`"E"`、`FizzBuzz16(15)`は`"FizzBuzz"`、`FizzBuzz16(16)`は`"10"`、`FizzBuzz16(251)`は`"FB"`になります  


`FizzBuzzString16`を次のように定義します (※擬似コード)
```
function FizzBuzzString16(n) {
    s := ""
    for i in 1 .. n {
        s += FizzBuzz16(i)
    }
    return s
}
```
例えば`FizzBuzzString16(4)`は`"12Fizz4"`、`FizzBuzzString16(16)`は`"12Fizz4BuzzFizz78FizzBuzzBFizzDEFizzBuzz10"`になります  


整数`X`と`Y`が与えられるので`FizzBuzzString16(1000000000000)`の`X`文字目から`Y`文字目の間にある文字`F`の個数と文字`B`の個数の合計を求めてください  
 
例えば`X=1,Y=5`のとき`FizzBuzzString16(1000000000000)`の`1`文字目から`5`文字目は`"12Fiz"`で文字`F`は`1`個、文字`B`は`0`個で合計は`1`となります  
例えば`X=3,Y=18`のとき`FizzBuzzString16(1000000000000)`の`3`文字目から`18`文字目は`"Fizz4BuzzFizz78F"`で文字`F`は`3`個、文字`B`は`1`個で合計は`4`となります  



#### Input Format

```
X
Y
```
1行目に整数`X`が与えられます  
2行目に整数`Y`が与えられます  
  
 
#### Constraints

整数`X`、`Y`の範囲は`1 <= X <= Y <= 1,000,000,000,000`です


#### Output Format

`FizzBuzzString16(1000000000000)`の`X`文字目から`Y`文字目の間にある文字`F`の個数と文字`B`の個数の合計を出力してください  


#### Sample #0

###### Input #0
```
1
5
```
###### Output #0
```
1
```
###### Explanation #0
`FizzBuzzString16(1000000000000)`の`1`文字目から`5`文字目は`"12Fiz"`で文字`F`は`1`個、文字`B`は`0`個で合計は`1`となります  


#### Sample #1

###### Input #1
```
3
18
```
###### Output #1  
```
4
```
###### Explanation #1
`FizzBuzzString16(1000000000000)`の`3`文字目から`18`文字目は`"Fizz4BuzzFizz78F"`で文字`F`は`3`個、文字`B`は`1`個で合計は`4`となります  


#### Sample #2

###### Input #2
```
33
33
```
###### Output #2
```
1
```
###### Explanation #2
`FizzBuzzString16(1000000000000)`の`33`文字目は`"F"`で文字`F`は`1`個、文字`B`は`0`個で合計は`1`となります  


#### Sample #3

###### Input #3
```
769
797
```
###### Output #3
```
11
```
###### Explanation #3
`FizzBuzzString16(1000000000000)`の`769`文字目から`797`文字目は`"F4BuzzFizzF7F8FizzBuzzFBFizzF"`で文字`F`は`8`個、文字`B`は`3`個で合計は`11`となります  


#### Sample #4

###### Input #4
```
128153
128159
```
###### Output #4
```
0
```
###### Explanation #4
`FizzBuzzString16(1000000000000)`の`128153`文字目から`128159`文字目は`"7777777"`で文字`F`も文字`B`もありません  


#### Sample #5

###### Input #5
```
808
277288
```
###### Output #5
```
52258
```


#### Sample #6

###### Input #6
```
1
1000000000000
```
###### Output #6
```
157481468199
```

